class WorkModel {
    #pay = 0;
    #bankAccount;
    #observers = [];
    /*
        A constructor for WorkModel. Bank should be of type BankModel.
        @constructor
    */
    constructor(bank) {
        this.#bankAccount = bank;
    }
    get pay() {
        return this.#pay;
    }
    set pay(value) {
        if (isNumber(value) === false) {
            console.error("Cannot set pay to non-number");
        }
        this.#pay = value;
        this.#notifyChange();
    }

    work() {
        this.pay += 100;
    }
    transferToBank() {
        // 10% of pay should pay off loan, if it exists.
        // If the loan is paid off with the 10% give the remainder to balance.
        if (this.#bankAccount.currentLoan > 0) {
            const tenPercentOfPay = this.pay * 0.1;
            this.pay -= tenPercentOfPay;
            this.#bankAccount.currentLoan -= tenPercentOfPay;
            this.#transferRemainingToBalance(this.pay);
        }

        this.#bankAccount.balance += this.pay;

        this.pay = 0;
    }

    repayLoan() {
        const remaining = this.pay - this.#bankAccount.currentLoan;
        this.#bankAccount.currentLoan -= this.pay;
        this.#transferRemainingToBalance(remaining);
        this.pay = 0;
    }
    // enables observers to attach to this object.
    addObserver(observer) {
        this.#observers.push(observer);
    }
    // Calls the update method of each observer.
    #notifyChange = () => {
        for (let observer of this.#observers) {
            if (typeof (observer.update) == "function") {
                observer.update();
            }
        }
    };
    #transferRemainingToBalance(remaining) {
        // If loan is not paid back, don't continue.
        if (this.#bankAccount.currentLoan > 0) return;
        if(remaining < 0) return;
        // const remaining = -this.#bankAccount.currentLoan;
        this.#bankAccount.balance += remaining;
    }
}
