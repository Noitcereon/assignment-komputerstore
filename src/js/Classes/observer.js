class ObserverModel{
    #update;
    constructor(updateCallback){
        this.#update = updateCallback;
    }

    update(){
        this.#update();
    }
}