class DataAccess {

    static fetchComputers() {
        const apiUrl = "https://noroff-komputer-store-api.herokuapp.com/computers";
        const output = fetch(apiUrl)
            .then(response => response.json())
            .then((data) => {
                return data;
            })
            .catch(error => {
                console.error(error);
            });
        return output;
    }
}