"use strict"
class BankModel {
    #balance;
    #currentLoan = 0;
    #observers = [];

    constructor(balance = 0) {
        this.balance = balance;
    }
    get balance() {
        return this.#balance;
    }
    set balance(value) {
        if (isNumber(value) === false) {
            console.error("Cannot set balance to non-numbers.");
            return;
        }
        this.#balance = value;
        this.#notifyChange();
    }
    get currentLoan() {
        return this.#currentLoan;
    }
    set currentLoan(value){
        if(isNumber(value) === false){
            console.error("Cannot set currentLoan to non-numbers");
            return;
        }
        this.#currentLoan = value;
        // prevent a negative loan.
        if(this.#currentLoan < 0) this.#currentLoan = 0;
        this.#notifyChange();
    }

    doLoanRequest() {
        let message; // feedback to user
        let loanAmount = prompt("How much do you want to loan?");
        loanAmount = parseFloat(loanAmount);
        if (isNumber(loanAmount) === false) {
            console.warn("User cancelled or provided invalid input.");
            message = "Cancelled action";
            return message;
        }
        // Check if balance is large enough for loanAmount.
        if (this.#balance * 2 < loanAmount) {
            message = `You can loan, at most, twice of your current balance (${this.#balance * 2})`;
            return message;
            // console.warn(`You can loan, at most, twice of your current balance (${this.#balance * 2})`);
        }
        // Check for existing loan.
        if (this.#currentLoan > 0) {
            message = "You need to pay back your first loan before getting another.";
            return message;
            // console.warn("You need to pay back your first loan before getting another.");
        }
        message = `You have loaned: ${loanAmount} DKK`;
        this.currentLoan = loanAmount;
        this.balance += loanAmount;

        return message;
    }
    // enables observers to attach to this object.
    addObserver(observer) {
        this.#observers.push(observer);
    }
    // Calls the update method of each observer.
    #notifyChange = () => {
        for (let observer of this.#observers) {
            if (typeof (observer.update) == "function") {
                observer.update();
            }
        }
    };
}