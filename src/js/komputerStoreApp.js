// #region Initial pageload setup
// Bank setup
const bank = new BankModel();

const bankObserver = new ObserverModel(() => {
    // handle UI updates in Bank area.
    bankCurrentBalanceSpan.innerText = `${bank.balance} Kr.`;
    bankCurrentLoanSpan.innerText = `${bank.currentLoan} Kr.`;
    if (bank.currentLoan > 0) {
        bankLoanDiv.classList.remove('hidden');
        workRepayLoanBtn.classList.remove('hidden');
        return;
    }
    bankLoanDiv.classList.add('hidden');
    workRepayLoanBtn.classList.add('hidden');
});
bank.addObserver(bankObserver);
bank.balance = 5;

// Work setup
const work = new WorkModel(bank);
workObserver = new ObserverModel(() => {
    // handle work UI updates in Work area.
    workCurrentPaySpan.innerText = `${work.pay} Kr.`;
});
work.addObserver(workObserver);
work.pay = 0;

// Laptops setup
let computers = [];
let selectedComputer;
DataAccess.fetchComputers()
    .then(computerData => {
        computers = computerData;
        for (const computer of computers) {
            const option = document.createElement("option");
            option.value = computer.id;
            option.innerText = computer.title;
            laptopsForSaleSelect.appendChild(option);
        }
    })
    .catch(error => console.error(error));

// Misc. setup
for (let element of currentYearElements) {
    element.innerText = new Date().getFullYear();
}

// #endregion Initial pageload setup

//#region Event Listeners
// Loan
bankGetLoanBtn.addEventListener("click", () => {
    let message = bank.doLoanRequest();

    // Show feedback to user.
    bankAlertDiv.innerText = message;
});

// Work and get money
workWorkBtn.addEventListener("click", () => {
    work.work();
});

// Transfer pay to bank
workBankBtn.addEventListener("click", () => {
    bankAlertDiv.innerText = "";
    work.transferToBank();
});
// Repay loan
workRepayLoanBtn.addEventListener("click", () => {
    work.repayLoan();
});

// Select laptop to display
laptopsForSaleSelect.addEventListener("change", () => {
    // get selected computer
    selectedComputer = computers.find(findSelectedComputer);

    // update all feature lists
    for (const unorderedList of laptopFeatureListsUl) {
        if (unorderedList.innerHTML !== "") {
            // reset list
            unorderedList.innerHTML = "";
        }
        addSpecsToUnorderList(unorderedList, selectedComputer.specs);
    }
    // display more info in the container below.
    laptopNameH2.innerText = selectedComputer.title;
    laptopDescriptionP.innerText = selectedComputer.description;
    laptopPriceTagSpan.innerText = `${selectedComputer.price} DKK`;
    const computerImgUrl = `https://noroff-komputer-store-api.herokuapp.com/${selectedComputer.image}`
    fetch(computerImgUrl).then((response) => {
        if (response.ok) {
            laptopImageImg.src = computerImgUrl;
        }
        else {
            laptopImageImg.src = "./assets/images/computer-placeholder-image.png"; // relative url to index.html
        }
    }).catch(error => {
        console.warn(error);
    });

    // #region Functions inside onchange 
    function addSpecsToUnorderList(ulHTMLObject, specs) {
        for (const spec of specs) {
            const listItem = document.createElement('li');
            listItem.innerText = spec;
            ulHTMLObject.appendChild(listItem);
        }
    }
    function findSelectedComputer(computer) {
        if (computer.id === Number(laptopsForSaleSelect.value)) return true;
    }
    //#endregion Functions inside onchange 
});


// Buy laptop
laptopBuyNowBtn.addEventListener("click", () => {
    if (bank.balance >= selectedComputer.price) {
        // perform buy action.
        bank.balance -= selectedComputer.price;
        alert(`You bought ${selectedComputer.title} for ${selectedComputer.price} DKK`);
        return;
    }
    alert(`You do not have enough money to buy this computer`);
});

//#endregion Event Listeners

