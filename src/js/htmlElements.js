// HTML elements from index.html

// "Bank" elements
const bankCurrentBalanceSpan = document.getElementById("currentBalance");
const bankLoanDiv = document.getElementById("bankLoanContainer");
const bankCurrentLoanSpan = document.getElementById("currentLoan");
const bankGetLoanBtn = document.getElementById("getLoanBtn");
const bankAlertDiv = document.getElementById("bankAlerts");

// "Work" elements
const workCurrentPaySpan = document.getElementById("workCurrentPay");
const workWorkBtn = document.getElementById("workWorkBtn");
const workBankBtn = document.getElementById("workBankBtn");
const workRepayLoanBtn = document.getElementById("workRepayLoanBtn");

// "Laptops" elements
const laptopsForSaleSelect = document.getElementById("laptopsForSaleSelect");

// "Laptop display" elements
const laptopBuyNowBtn = document.getElementById("laptopBuyNowBtn");
const laptopNameH2 = document.getElementById("laptopName");
const laptopDescriptionP = document.getElementById("laptopDescription");
const laptopPriceTagSpan = document.getElementById("laptopPriceTag");
const laptopImageImg = document.getElementById("laptopImage");

// Global elements
const currentYearElements = document.getElementsByClassName("currentYear"); // array
const laptopFeatureListsUl = document.getElementsByClassName("laptopFeatureList"); // array

