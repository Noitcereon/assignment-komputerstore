// This file contains reusable helper functions, such as checks.


function isNumber(numberToCheck) {
    if (isNaN(numberToCheck) || typeof (numberToCheck) !== "number") {
        return false;
    }
    return true;
}