

<h1 align="center">
  Komputer Store App
  <br>
</h1>

<h4 align="center">An assignment covering the fundamentals of HTML, CSS and JavaScript (no frameworks).</h4>

<p align="center">
  <a href="#how-to-use">How To Use</a> •
  <a href="https://noitcereon.gitlab.io/assignment-komputerstore/">Online version hosted on GitLab Pages</a> •
  <a href="#license">License</a>
</p>

## Preview
![screenshot](https://i.imgur.com/kMka5oT.png)


## How To Use

```bash
# Clone this repository
$ git clone https://github.com/amitmerchant1990/electron-markdownify

# Use it
$ Open the index.html

```
## Credits
- Read me template made by GitHub [@amitmerchant1990](https://github.com/amitmerchant1990) &nbsp;&middot;&nbsp;

## License

MIT

---

> GitHub [@noitcereon](https://github.com/noitcereon)

